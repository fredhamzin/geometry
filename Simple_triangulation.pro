HEADERS += \
    src/algo/PolygonInputParser.h \
    src/gui/MainWidget.h \
    src/gui/Drawer.h \
    src/objects/Point2D.h \
    src/objects/Graph.h \
    src/objects/Triangle.h \
    src/objects/Line.h \
    src/objects/ConvexHull.h \
    src/algo/Triangulation.h

SOURCES += \
    src/algo/main.cpp \
    src/gui/MainWidget.cpp \
    src/gui/Drawer.cpp \
    src/objects/Point2D.cpp \
    src/objects/Line.cpp \
    src/objects/Triangle.cpp \
    src/objects/ConvexHull.cpp \
    src/algo/Triangulation.cpp
CONFIG += debug

#include "src/algo/Triangulation.h"


//function for sorting points by X coordinate and initializing index of point
std::vector <Point2D> sort_vertices(std::vector <Point2D> points){
    std::sort(points.begin(),points.end());
    for (int i = 0; i < points.size(); ++i)
    {
        points[i].index = i;
    }
    return points;
}

//fuction to get edge of connected vertices
std::pair <int, int> get_edge(Point2D p1, Point2D p2){
    std::pair <int, int> e;
    return e;
}


//result triangulation of vertices as graph
Graph triangulate(Graph g){
    unsigned int connect_point = 0;
    long long int l_check = 0;
    bool continue_fl = false;
    int check1, check2;
    unsigned int num, num1, num2;
    ConvexHull ch;
    Line line;
    Triangle triangle;

    g.vertices=sort_vertices(g.vertices);
    g.edges.clear();
    g.addEdge(0,1);
    ch.addToLeft(0);
    ch.addToLeft(1);
    ch.addToRight(0);
    ch.addToRight(1);
    ch.root=1;

    unsigned int ind = 2;
    while (ind < g.vertices.size())
    {
        ch.l=--ch.left_l.end();
        ch.r=--ch.right_l.end();
        g.addEdge(ind, ch.root);
        line.setNewLine(g.vertices[ch.root], g.vertices[ind]);
        num1 = *(--ch.l);
        num2 = *(--ch.r);
        check1 = line.point_location(g.vertices[num1]);
        check2 = line.point_location(g.vertices[num2]);
        if (((check1==1) && (check2==1)) || ((check1==0) && (check2==1)))  //root is right supporting point
        {
            ch.addToLeft(ind);

            num = *ch.r;
            if (num == ch.m)
            {
                g.addEdge(ind, num);
                triangle.setNewTriangle(g.vertices[ind], g.vertices[ch.root], g.vertices[num]);
                g.triangles.push_back(triangle);
                //deleting
                ++ch.r;
                ch.right_l.erase(ch.r, ch.right_l.end());
                ch.addToRight(ind);
            }
            else
            {
                continue_fl = true;
                while (continue_fl) //while left supporting point is not found
                {
                    g.addEdge(ind, num);
                    num1 = *(--ch.r);
                    ++ch.r;
                    num2 = *(++ch.r);
                    --ch.r;
                    triangle.setNewTriangle(g.vertices[ind], g.vertices[num], g.vertices[num2]);
                    g.triangles.push_back(triangle);
                    line.setNewLine(g.vertices[num], g.vertices[ind]);
                    check1 = line.point_location(g.vertices[num1]);
                    check2 = line.point_location(g.vertices[num2]);
                    if ((check1==-1) && (check2==-1)) //left supporting point was found
                    {
                        //deleting and setting new root
                        ++ch.r;
                        ch.right_l.erase(ch.r, ch.right_l.end());
                        ch.addToRight(ind);
                        continue_fl = false;
                        break;
                    }
                    else
                    {
                        if (num1 == ch.m)
                        {
                            g.addEdge(ind, num1);
                            //deleting
                            triangle.setNewTriangle(g.vertices[ind], g.vertices[num1], g.vertices[num]);
                            g.triangles.push_back(triangle);
                            ch.right_l.erase(ch.r, ch.right_l.end());
                            ch.addToRight(ind);
                            continue_fl = false;
                            //g.addEdge(ind, num1);
                            break;
                        }
                        else
                        {
                            num = *(--ch.r);
                        }
                    }
                }
            }

        }
        else if (((check1==-1) && (check2==-1)) || ((check1 == 0) && (check2 == -1))) //root is left supporting point
        {
            ch.addToRight(ind);
            num = *ch.l;
            if (num == ch.m)
            {
                g.addEdge(ind, num);
                triangle.setNewTriangle(g.vertices[ind], g.vertices[ch.root], g.vertices[num]);
                g.triangles.push_back(triangle);
                //deleting
                ++ch.l;
                ch.left_l.erase(ch.l, ch.left_l.end());
                ch.addToLeft(ind);
            }
            else
            {
                continue_fl = true;
                while (continue_fl) //while left supporting point is not found
                {
                    g.addEdge(ind, num);
                    num1 = *(--ch.l);
                    ++ch.l;
                    num2 = *(++ch.l);
                    --ch.l;
                    triangle.setNewTriangle(g.vertices[ind], g.vertices[num], g.vertices[num2]);
                    g.triangles.push_back(triangle);
                    line.setNewLine(g.vertices[num], g.vertices[ind]);
                    check1 = line.point_location(g.vertices[num1]);
                    check2 = line.point_location(g.vertices[num2]);
                    if ((check1==1) && (check2==1)) //right supporting point was found
                    {
                        //deleting and setting new root
                        ++ch.l;
                        ch.left_l.erase(ch.l, ch.left_l.end());
                        ch.addToLeft(ind);
                        continue_fl = false;
                        break;
                    }
                    else
                    {
                        if (num1 == ch.m)
                        {
                            g.addEdge(ind, num1);
                            triangle.setNewTriangle(g.vertices[ind], g.vertices[num1], g.vertices[num]);
                            g.triangles.push_back(triangle);
                            //deleting
                            ch.left_l.erase(ch.l, ch.left_l.end());
                            ch.addToLeft(ind);
                            continue_fl = false;
                            //g.addEdge(ind, num1);
                            break;
                        }
                        else
                        {
                            num = *(--ch.l);
                        }
                    }
                }
            }
        }
        else if (((check1 == 1) && (check2 == -1)) || ((check1 == -1) && (check2 == 1)))
        {
            num = *ch.r;
            if (num == ch.m)
            {
                g.addEdge(ind, num);
                triangle.setNewTriangle(g.vertices[ind], g.vertices[ch.root], g.vertices[num]);
                g.triangles.push_back(triangle);
                //deleting
                ++ch.r;
                ch.right_l.erase(ch.r, ch.right_l.end());
                ch.addToRight(ind);
            }
            else
            {
                continue_fl = true;
                while (continue_fl) //while left supporting point is not found
                {
                    g.addEdge(ind, num);
                    num1 = *(--ch.r);
                    ++ch.r;
                    num2 = *(++ch.r);
                    --ch.r;
                    triangle.setNewTriangle(g.vertices[ind], g.vertices[num], g.vertices[num2]);
                    g.triangles.push_back(triangle);
                    line.setNewLine(g.vertices[num], g.vertices[ind]);
                    check1 = line.point_location(g.vertices[num1]);
                    check2 = line.point_location(g.vertices[num2]);
                    if ((check1==-1) && (check2==-1)) //left supporting point was found
                    {
                        //deleting and setting new root
                        ++ch.r;
                        ch.right_l.erase(ch.r, ch.right_l.end());
                        ch.addToRight(ind);
                        continue_fl = false;
                        break;
                    }
                    else
                    {
                        if (num1 == ch.m)
                        {
                            g.addEdge(ind, num1);
                            //deleting
                            triangle.setNewTriangle(g.vertices[ind], g.vertices[num1], g.vertices[num]);
                            g.triangles.push_back(triangle);
                            ch.right_l.erase(ch.r, ch.right_l.end());
                            ch.addToRight(ind);
                            continue_fl = false;
                            //g.addEdge(ind, num1);
                            break;
                        }
                        else
                        {
                            num = *(--ch.r);
                        }
                    }
                }
            }


            //now we will find right supporting point
            num = *ch.l;
            if (num == ch.m)
            {
                g.addEdge(ind, num);
                triangle.setNewTriangle(g.vertices[ind], g.vertices[ch.root], g.vertices[num]);
                g.triangles.push_back(triangle);
                //deleting
                ++ch.l;
                ch.left_l.erase(ch.l, ch.left_l.end());
                ch.addToLeft(ind);
            }
            else
            {
                continue_fl = true;
                while (continue_fl) //while left supporting point is not found
                {
                    g.addEdge(ind, num);
                    num1 = *(--ch.l);
                    ++ch.l;
                    num2 = *(++ch.l);
                    --ch.l;
                    triangle.setNewTriangle(g.vertices[ind], g.vertices[num], g.vertices[num2]);
                    g.triangles.push_back(triangle);
                    line.setNewLine(g.vertices[num], g.vertices[ind]);
                    check1 = line.point_location(g.vertices[num1]);
                    check2 = line.point_location(g.vertices[num2]);
                    if ((check1==1) && (check2==1)) //right supporting point was found
                    {
                        //deleting and setting new root
                        ++ch.l;
                        ch.left_l.erase(ch.l, ch.left_l.end());
                        ch.addToLeft(ind);
                        continue_fl = false;
                        break;
                    }
                    else
                    {
                        if (num1 == ch.m)
                        {
                            g.addEdge(ind, num1);
                            triangle.setNewTriangle(g.vertices[ind], g.vertices[num1], g.vertices[num]);
                            g.triangles.push_back(triangle);
                            //deleting
                            ch.left_l.erase(ch.l, ch.left_l.end());
                            ch.addToLeft(ind);
                            continue_fl = false;
                            //g.addEdge(ind, num1);
                            break;
                        }
                        else
                        {
                            num = *(--ch.l);
                        }
                    }
                }
            }

        }
        else //check1==0, check2==0 or first set of consecutive points lie on one line
        {
            ch.addToLeft(ind);
            ch.addToRight(ind);
            //ch.root = ind;
        }
        ch.root = ind;
        ++ind;
    }

    return g;
}



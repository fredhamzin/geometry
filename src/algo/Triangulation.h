#ifndef TRIANGULATE_H
#define TRIANGULATE_H
#include <algorithm>
#include <set>
#include <cstdio>
#include <math.h>
#include "src/objects/ConvexHull.h"
#include "src/objects/Graph.h"
#include "src/objects/Triangle.h"

//function for points sort by X coordinate
std::vector <Point2D> sort_vertices(std::vector <Point2D> points);

//fuction to get edge of connected vertices
std::pair <int, int> get_edge(Point2D p1, Point2D p2);

//function to check is 3rd point lefter from line through first and second points
long long int is_left(Point2D p1, Point2D p2, Point2D p3);

//result triangulation of vertices as graph
Graph triangulate(Graph g);

#endif //


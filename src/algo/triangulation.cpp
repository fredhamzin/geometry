#include "src/algo/triangulation.h"


//function for points sort by X coordinate
std::vector <Point2D> sort_vertices(std::vector <Point2D> points){
    std::sort(points.begin(),points.end());
    return points;
}

//fuction to get edge of connected vertices
std::pair <int, int> get_edge(Point2D p1, Point2D p2){
    std::pair <int, int> e;
    return e;
}

//function to check is 3rd point lefter from line through first and second points
long long int is_left(Point2D p1, Point2D p2, Point2D p3)
{
    long long int res = ((long long int)p2.x - (long long int)p1.x);
    res *= ((long long int)p3.y - (long long int)p1.y);
    long long int res2 =  ((long long int)p3.x - (long long int)p1.x);
    res2 *=  ((long long int)p2.y - (long long int)p1.y);
    long long int res3 = res - res2;
    return res3;
}

//result triangulation of vertices as graph
Graph triangulate(Graph g){
    unsigned int connect_point = 0;
    long long int l_check = 0;
    bool flag_left = false;
    g.vertices=sort_vertices(g.vertices);
    g.edges.clear();
    g.addEdge(0,1);

    unsigned int ind = 2;
    while (ind < g.getVertexCount()){
        l_check = is_left(g.vertices[ind-2], g.vertices[ind-1], g.vertices[ind]);
        g.addEdge(ind,ind-1);
        if (l_check == 0){
            g.addEdge(ind,connect_point);
            flag_left=true;
        }
        else if (l_check > 0){
            if (flag_left){
                g.addEdge(ind,connect_point);
            }
            else{
                flag_left = true;
                connect_point = ind - 2;
                g.addEdge(ind,connect_point);
            }
        }
        else {
            if (!flag_left){
                g.addEdge(ind,connect_point);
            }
            else{
                flag_left=false;
                connect_point = ind - 2;
                g.addEdge(ind,connect_point);
            }
        }
        Triangle t = Triangle(g.vertices[connect_point],g.vertices[ind-1],g.vertices[ind]);
        g.triangles.push_back(t);
        ++ind;
    }
    return g;
}



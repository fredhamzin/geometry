#ifndef CONVEXHULL_H
#define CONVEXHULL_H
#include <list>
#include "src/objects/Point2D.h"
#include "src/objects/Line.h"

class ConvexHull {
public:
    unsigned int root; //index of point with the most X coordinate
    static const unsigned int m = 0; //index of the most left point
    std::list<unsigned int>::iterator l; //left supporting point
    std::list<unsigned int>::iterator r; //right supporting point
    std::list<unsigned int> left_l;
    std::list<unsigned int> right_l;
    void addToLeft(unsigned int);
    void addToRight(unsigned int);
    //void findSupPoint(Point2D p);
};


#endif // CONVEXHULL_H

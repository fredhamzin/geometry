#include "Point2D.h"

Point2D::Point2D ()  :
    x(0),
    y(0),
    type(0),
    index(-1) {
}


Point2D::Point2D (int x, int y) :
    x (x),
    y (y),
    type (0),
    index (-1) {
}


Point2D::Point2D (const Point2D &arg) :
    x (arg.x),
    y (arg.y),
    type (arg.type),
    index (arg.index) {
}


Point2D& Point2D::operator= (const Point2D &arg) {
    x = arg.x;
    y = arg.y;
    type = arg.type;
    index = index;
    return *this;
}


Point2D Point2D::operator - (const Point2D &arg) {
    Point2D res (*this);
    return res -= arg;
}


Point2D& Point2D::operator -= (const Point2D &arg) {
    x -= arg.x;
    y -= arg.y;

    return *this;
}

bool operator ==(const Point2D& a, const Point2D& b)
{
    if(a.x == b.x && a.y == b.y)
        return 1;
    else
        return 0;
}
bool operator <(const Point2D& a, const Point2D& b)
{
    if(a.x < b.x)
        return 1;
    else if(a.x == b.x && a.y < b.y)
        return 1;
    else
        return 0;
}
void Point2D::setType (int t)
{
    type = t;
}

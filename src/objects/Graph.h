#ifndef GRAPH_H
#define GRAPH_H

#include <cassert>
#include <utility>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "src/objects/Point2D.h"
#include "src/objects/Triangle.h"


class Graph {
public:
    Graph() {}


    Graph(std::vector <Point2D> data) :
        vertices (data) {
    }


    Graph (std::vector <Point2D> vertices, std::vector <std::pair<int, int> > edges, std::vector <Triangle> triangles) :
        vertices (vertices),
        edges (edges),
        triangles (triangles){
    }


    Graph (const Graph &arg) :
        vertices (arg.vertices),
        edges (arg.edges),
        triangles (arg.triangles){
    }


    Graph& operator = (const Graph &arg) {
        edges = arg.edges;
        vertices = arg.vertices;
        triangles.clear();
        for (int i = 0; i < arg.triangles.size(); ++i){
            triangles.push_back(arg.triangles[i]);
        }
        //triangles = arg.triangles;

        return *this;
    }


    void addEdge (unsigned int a, unsigned int b) {
        edges.push_back(std::pair<int, int>(a, b));
    }


    void addVertex (Point2D pt) {
        vertices.push_back(pt);
    }


    void clear() {
        edges.clear();
        vertices.clear();
    }


    //become a polygon
    void connectVertices() {
        edges.clear();

        for (unsigned int i = 0; i < vertices.size(); ++i) {
            edges.push_back(std::pair<int, int> (i, i + 1));
        }

        if (!edges.empty()) {
            edges.pop_back();
            edges.push_back(std::pair<int, int> (vertices.size() - 1, 0));
        }
    }

    void dumpVertices (std::string fileName) {
        std::ofstream outp (fileName.c_str());
        if (outp.fail()) {
            std::cerr << "can't open output file: " << fileName << std::endl;
            return;
        }


        outp << "TRIANGLES - FORMAT OF OUTPUT:" << std::endl;
        outp << "POINT1 POINT2 POINT3" << std::endl;

        for (unsigned int i = 0; i < triangles.size(); ++i) {
            outp << triangles[i].v1.getX() << ' ' << triangles[i].v1.getY() << ' ';
            outp << ' ' << triangles[i].v2.getX() << ' ' << triangles[i].v2.getY() << ' ';
            outp << ' ' << triangles[i].v3.getX() << ' ' << triangles[i].v3.getY() << std::endl;
        }

        outp.close();
    }


    Point2D getVertex (unsigned int i) const {
        assert (i < vertices.size());
        return vertices[i];
    }


    unsigned int getVertexCount () const {
        return vertices.size();
    }


    std::pair <int, int> getEdge (unsigned int i) const {
        assert (i < edges.size());
        return edges[i];
    }


    unsigned int getEdgeCount () const {
        return edges.size();
    }


    void removePoint (unsigned int index) {
        if (index < vertices.size()) {
            vertices.erase(vertices.begin() + index, vertices.begin() + index + 1);
            connectVertices();
        }
    }

    std::vector <Triangle> triangles;
    std::vector <Point2D> vertices;
    std::vector <std::pair<int, int> > edges;    //pair contains numbers of two incident vertexes
};

#endif // GRAPH_H

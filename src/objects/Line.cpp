#include "Line.h"

Line::Line(){
}
Line::Line(Point2D p1, Point2D p2){
    this->p1 = p1;
    this->p2 = p2;
}

void Line::setNewLine(Point2D p1, Point2D p2)
{
    this->p1 = p1;
    this->p2 = p2;
}

//fuction return 1 if point p locates lefter than line, 0 if the same and -1 if it's righter
int Line::point_location(Point2D p){
    long long res1 = ((long long int)p2.x - (long long int)p1.x);
    res1 *= ((long long int)p.y - (long long int)p1.y);
    long long int res2 = ((long long int)p2.y - (long long int)p1.y);
    res2 *= ((long long int)p.x - (long long int)p1.x);
    long long int res3 = res1 - res2;
    if (res3 > 0) return 1; //lefter
    else if (res3 < 0) return -1; //righter
    else return 0;//same
}

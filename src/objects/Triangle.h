#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "src/objects/Point2D.h"


class Triangle {
public:
    Point2D v1;
    Point2D v2;
    Point2D v3;

    Triangle();
    Triangle(Point2D v1, Point2D v2, Point2D v3);
    Triangle(const Triangle& arg);

    Triangle& operator = (const Triangle &arg);

    void setNewTriangle(Point2D v1, Point2D v2, Point2D v3);

};

#endif // TRIANGLE_H

#include "Triangle.h"

Triangle::Triangle(){
}

Triangle::Triangle(Point2D v1, Point2D v2, Point2D v3){
    this->v1=v1;
    this->v2=v2;
    this->v3=v3;
}

Triangle::Triangle(const Triangle& arg) :
    v1 (arg.v1),
    v2 (arg.v2),
    v3 (arg.v3){
}

Triangle& Triangle::operator = (const Triangle &arg) {
    v1 = arg.v1;
    v2 = arg.v2;
    v3 = arg.v3;

    return *this;
}

void Triangle::setNewTriangle(Point2D v1, Point2D v2, Point2D v3)
{
    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;
}

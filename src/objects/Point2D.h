#ifndef POINT2D_H
#define POINT2D_H

class Point2D {
public:
    int x;
    int y;
    int type; //-1 - reflex type, 1 - supporting type, 0 - not initialized
    //we needn't concave type because we looking for only visible part of convex hull
    int index;

    Point2D ();
    Point2D (int, int);
    Point2D (const Point2D &arg);

    Point2D& operator = (const Point2D &arg);
    Point2D operator - (const Point2D &arg);
    Point2D& operator -= (const Point2D &arg);

    friend bool operator ==(const Point2D& a, const Point2D& b);
    friend bool operator <(const Point2D& a, const Point2D& b);

    int getX () {return x;}
    int getY () {return y;}
    void setType (int t);

};


#endif // POINT2D_H

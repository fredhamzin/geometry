#ifndef LINE_H
#define LINE_H

#include "src/objects/Point2D.h"
#include <iostream>

class Line{
public:
    Point2D p1;
    Point2D p2;
    Line();
    Line(Point2D p1, Point2D p2);

    void setNewLine(Point2D p1, Point2D p2);

    //fuction return 1 if point p locates lefter than line, 0 if the same and -1 if it's righter
    int point_location(Point2D p);
};

#endif // LINE_H
